package space.neothefox.laytray;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

public class IconBuilderPreview extends View
{

    private int mode;
    private int textSize;
    private boolean fakeBold;
    private int textColor;

    public IconBuilderPreview(Context context)
    {
        super(context);
        initValues();
    }

    public IconBuilderPreview(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        initValues();
    }

    public IconBuilderPreview(Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        initValues();
    }

    public IconBuilderPreview(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        initValues();
    }

    private void initValues()
    {
        textSize = 48 * 10;
        fakeBold = true;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        mode = 2;
        fakeBold = true;
        int width = 48 * 10;
        int height = 48 * 10;
        textColor = Color.WHITE;
        String text = "EN";

        this.setLayoutParams(new LinearLayout.LayoutParams(height, width));

        Paint paint = new Paint(ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
        paint.setTextSize(textSize);
        paint.setFakeBoldText(fakeBold);
        paint.setColor(textColor);
        switch (mode)
        {
            case 1: {

                canvas.drawCircle(width / 2f, height / 2f, width / 2f, paint);
                //paint.setAlpha(255);
                paint.setColor(Color.BLACK);
                paint.setTextSize(textSize);
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText(text, width / 2f, height / 2f + textSize / 2f, paint);
                return;
            }

            case 2: {

                canvas.drawRoundRect(new RectF(0, 0, height, width), 5, 5, paint);
                //paint.setAlpha(255);
                paint.setColor(Color.BLACK);
                paint.setTextSize(textSize);
                paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText(text, width / 2f, height / 2f + textSize / 2f, paint);
                return ;
            }

            case 0:
            default:
                paint.setTextAlign(Paint.Align.LEFT);
                float baseline = -paint.ascent(); // ascent() is negative
                width = (int) (paint.measureText(text) + 0.5f); // round
                height = (int) (baseline + paint.descent() + 0.5f);
                Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                canvas.drawText(text, 0, baseline, paint);
                return;
        }
    }

    public int getMode()
    {
        return mode;
    }

    public int getTextSize()
    {
        return textSize;
    }

    public boolean isFakeBold()
    {
        return fakeBold;
    }

    public int getTextColor()
    {
        return textColor;
    }

    public void setMode(int mode)
    {
        this.mode = mode;
    }

    public void setTextSize(int textSize)
    {
        this.textSize = textSize * 10;
    }

    public void setFakeBold(boolean fakeBold)
    {
        this.fakeBold = fakeBold;
    }

    public void setTextColor(int textColor)
    {
        this.textColor = textColor;
    }
}
